from tkinter import *
from tkinter import messagebox
from random import choice, randint, shuffle
import pyperclip


# ---------------------------- My functions ------------------------------- #
def empty_fields(mydict):
    errors = ''
    for key, value in mydict.items():
        if not value:
            errors += f"'{key}' must not empty\n"
    return errors


def reset_form():
    website_entry.delete(0, END)
    email_entry.delete(0, END)
    password_entry.delete(0, END)
    email_entry.insert(0, 'fvasquez@local.com')


def generate_password():
    password_entry.delete(0, END)
    password = encrypt_password()
    password_entry.insert(0, password)
    pyperclip.copy(password)


# ---------------------------- PASSWORD GENERATOR ------------------------------- #
def encrypt_password():
    letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
               's', 't', 'u','v','w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
               'K', 'L', 'M', 'N', 'O', 'P', 'Q','R','S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
    numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    symbols = ['!', '#', '$', '%', '&', '(', ')', '*', '+']

    password_list = [choice(letters) for _ in range(randint(8, 10))]
    password_list += [choice(symbols) for _ in range(randint(2, 4))]
    password_list += [choice(numbers) for _ in range(randint(2, 4))]

    shuffle(password_list)

    return ''.join(password_list)


# ---------------------------- SAVE PASSWORD ------------------------------- #
def save():
    mydict = {'Website': website_entry.get(), "Email": email_entry.get(), "Password": password_entry.get()}
    errors = empty_fields(mydict)
    if not errors:
        result = f"{mydict['Website']} | {mydict['Email']} | {mydict['Password']}"
        is_ok = messagebox.askokcancel(
            message='Are you sure you want to save info?', icon='question', title='Install')
        if is_ok:
            with open('info.txt', 'a') as file:
                file.write(result + '\n')
                reset_form()
    else:
        messagebox.showinfo(message=errors)


# ---------------------------- UI SETUP ------------------------------- #
window = Tk()
window.title('Password Manager')
window.config(padx=50, pady=50)

canvas = Canvas(width=200, height=200)
logo_img = PhotoImage(file='logo.png')
canvas.create_image(100, 100, image=logo_img)
canvas.grid(row=0, column=1)

# --- Labels
website_lb = Label(text="Website:")
website_lb.grid(row=1, column=0)
email_lb = Label(text="Email/Username:")
email_lb.grid(row=2, column=0)
password_lb = Label(text="Password:")
password_lb.grid(row=3, column=0)

# --- Entries

website_entry = Entry(width=46)
website_entry.grid(row=1, column=1, columnspan=2)
website_entry.focus()

email_entry = Entry(width=46)
email_entry.grid(row=2, column=1, columnspan=2)
email_entry.insert(0, "fvasquez@local.com")
password_entry = Entry(width=27)
password_entry.grid(row=3, column=1)

# --- Buttons
generate_btn = Button(text='Generate Password', command=generate_password)
generate_btn.grid(row=3, column=2)
add_btn = Button(text='Add', width=43, command=save)
add_btn.grid(row=4, column=1, columnspan=2)

window.mainloop()
